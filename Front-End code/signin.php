<!DOCTYPE html>
<html>
<head>
	<title>Log-in RR-TICKET-SYSTEM</title>
	<style type="text/css">
		* { margin: 0px; padding: 0px; }
		body {
			font-size: 120%;
			background: #F8F8FF;
		}
		.header {
			width: 40%;
			margin: 50px auto 0px;
			color: white;
			background: #5F9EA0;
			text-align: center;
			border: 1px solid #B0C4DE;
			border-bottom: none;
			border-radius: 10px 10px 0px 0px;
			padding: 20px;
		}
		form, .content {
			width: 40%;
			margin: 0px auto;
			padding: 20px;
			border: 1px solid #B0C4DE;
			background: white;
			border-radius: 0px 0px 10px 10px;
		}
		.input-group {
			margin: 10px 0px 10px 0px;
		}
		.input-group label {
			display: block;
			text-align: left;
			margin: 3px;
		}
		.input-group input {
			height: 30px;
			width: 93%;
			padding: 5px 10px;
			font-size: 16px;
			border-radius: 5px;
			border: 1px solid gray;
		}
		.btn {
			padding: 10px;
			font-size: 15px;
			color: white;
			background: #5F9EA0;
			border: none;
			border-radius: 5px;
		}
	</style>
</head>
<body>
<div class="header">
	<h2>Log-In</h2>
</div>
<form method="post" action="check.php">
	<div class="input-group">
		<label>Email</label>
		<input type="email" name="email">
	<div class="input-group">
		<label>Password</label>
		<input type="password" name="password_1">
	</div>
	<div class="input-group">
		<button type="submit" class="btn" name="register_btn">Log in</button>
	</div>
</form>
</body>
</html>