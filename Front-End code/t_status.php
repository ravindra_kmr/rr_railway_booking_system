<!DOCTYPE html>
<html>
<head>
	<title>Answer-Query</title>
	<style type="text/css">
		body {
			width: 100wh;
			height: 90vh;
			color: #fff;
			background: linear-gradient(-40deg, #EE7752, #E73C7E, #23A6D5, #23D5AB);
			background-size: 400% 400%;
			-webkit-animation: Gradient 7s ease infinite;
			-moz-animation: Gradient 7s ease infinite;
			animation: Gradient 7s ease infinite;
		}

		@-webkit-keyframes Gradient {
			0% {
				background-position: 0% 50%
			}
			50% {
				background-position: 100% 50%
			}
			100% {
				background-position: 0% 50%
			}
		}

		@-moz-keyframes Gradient {
			0% {
				background-position: 0% 50%
			}
			50% {
				background-position: 100% 50%
			}
			100% {
				background-position: 0% 50%
			}
		}

		@keyframes Gradient {
			0% {
				background-position: 0% 50%
			}
			50% {
				background-position: 100% 50%
			}
			100% {
				background-position: 0% 50%
			}
		}


		table {
		padding-top: 300px;
		padding-left: 500px;
		margin: 8px;
		}

		th {
		font-family: Arial, Helvetica, sans-serif;
		font-size: 1em;
		color: #FFF;
		padding: 2px 6px;
		border-collapse: separate;
		border: 1px solid #000;
		}

		td {
		font-family: Arial, Helvetica, sans-serif;
		font-size: 1em;
		border: 1px solid #DDD;
		}
	</style>
</head>
<body>
<?php
		$t_id = $_POST["t_id"];
		$date = $_POST["date"];
		$s = $_POST["source"];
		$d = $_POST["destination"];
		$c = $_POST["class"];
		$servername = "localhost";
		$username = "root";
		$password = "DON";
		$dbname = "railway";
		// Create connection
		$conn = mysqli_connect($servername, $username, $password, $dbname);
		// Check connection
		if (!$conn) {
		die("Connection failed: " . mysqli_connect_error());
		}
		$sql = "call free_seats($t_id,$s,$d,$c,'$date');";
		$result = mysqli_query($conn, $sql);

		echo "<table border='1'>
    	<thead>
        <tr>
            <th>Empty Seats</th>
            <th>Total Seats</th>
        </tr>
    	</thead>";

		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_assoc($result)) {
				echo "<tr>";
				echo "<td>" . $row['@seat_f'] . "</td>";
				echo "<td>" . $row['@max_seat'] . "</td>";
				echo "</tr>";
			}
			echo "</table>";
		} 
		else {
			echo "<tr>";
			echo "<td>" . "No result" . "</td>";
			echo "<td>" . "No result" . "</td>";
			echo "</tr>";
		}

		mysqli_close($conn);
	?>
</body>
</html>

