<?php 
	session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Cancel</title>
	<style type="text/css">
		ul {
	    list-style-type: none;
	    margin: 0;
	    padding: 0;
	    overflow: hidden;
	    background-color: #333;
		}

		li {
		    float: left;
		}

		li a {
		    display: block;
		    color: white;
		    text-align: center;
		    padding: 14px 16px;
		    text-decoration: none;
		}

		li a:hover:not(.active) {
		    background-color: #111;
		}

		.active {
		    background-color: #4CAF50;
		}
		body {
			width: 100wh;
			height: 90vh;
			color: #fff;
			background: linear-gradient(-40deg, #EE7752, #E73C7E, #23A6D5, #23D5AB);
			background-size: 400% 400%;
			-webkit-animation: Gradient 7s ease infinite;
			-moz-animation: Gradient 7s ease infinite;
			animation: Gradient 7s ease infinite;
		}

		@-webkit-keyframes Gradient {
			0% {
				background-position: 0% 50%
			}
			50% {
				background-position: 100% 50%
			}
			100% {
				background-position: 0% 50%
			}
		}

		@-moz-keyframes Gradient {
			0% {
				background-position: 0% 50%
			}
			50% {
				background-position: 100% 50%
			}
			100% {
				background-position: 0% 50%
			}
		}

		@keyframes Gradient {
			0% {
				background-position: 0% 50%
			}
			50% {
				background-position: 100% 50%
			}
			100% {
				background-position: 0% 50%
			}
		}

		h1,
		h6 {
			font-family: 'Open Sans';
			font-weight: 300;
			text-align: center;
			position: ;
			top: 45%;
			right: 0;
			left: 0;
		}

		.first {
			display: inline-block;
			margin-top: 250px;
			margin-left: 250px;
		}
	
		.button {
		  border-radius: 4px;
		  background-color: #E73C7E;
		  border: none;
		  color: #FFFFFF;
		  text-align: center;
		  font-size: 20px;
		  padding: 10px;
		  width: 260px;
		  transition: all 0.5s;
		  cursor: pointer;
		  margin: 5px;
		}

		.button span {
		  cursor: pointer;
		  display: inline-block;
		  position: relative;
		  transition: 0.5s;
		}

		.button span:after {
		  content: '\00bb';
		  position: absolute;
		  opacity: 0;
		  top: 0;
		  right: -20px;
		  transition: 0.5s;
		}

		.button:hover span {
		  padding-right: 25px;
		}

		.button:hover span:after {
		  opacity: 1;
		  right: 0;
		}
	</style>
</head>
<body>
	<ul>
	  <li><a href="test11.php">Home</a></li>
	  <li><a href="test3.php">Book</a></li>
	  <li><a href="test111.php">Query</a></li>
	  <li><a href="test4.php">Cancel Ticket</a></li>
	  <li><a class="active" href="pnr.php">Book History and PNR Status</a></li>
	  <li><a href="test2.php">About</a></li>
	  <li style="float:right"><a href=""><?php echo $_SESSION['login_user']; ?></a></li>
	  <li style="float:right"><a href="/logout.php">Log out</a></li>
	</ul>

	<form class = 'first' action = "history.php" method="post">
	<fieldset>
	  <legend>Booking History</legend>
	  Check my Booking History:<br>
	  <input type="submit" value="Check">
	  </fieldset>
	</form>
	<form class = 'first' action = "pnrstatus.php" method="post">
	<fieldset>
	  <legend>PNR Status</legend>
	  PNR:<br>
	  <input type="number" name="pnr">
	  <br><br>
	  <input type="submit" value="Check">
	  </fieldset>
	</form>

</body>
</html>